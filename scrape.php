<?php

require_once 'WikipediaStorer.class.php';
require_once 'WikipediaFetcher.class.php';

$wps = new WikipediaStorer;

foreach (file('data.fr.csv', FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES) as $line) {
	list($section, $page) = explode(',', $line, 2);
	$wpp = new WikipediaFetcher($page, $section);
	echo "Fetching {$page} into {$section}...";
	$wpp->get();
	//$wpp->getFake();
	$s = $wps->store($wpp);
	$size = strlen($s->html);
	$n = rand(1, 10);
	echo " got $size bytes, waiting $n seconds\n";
	sleep($n);
}
