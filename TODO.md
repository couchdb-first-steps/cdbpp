TODO
====

Remplacer tout le PHP
---------------------
1) scraper wikipedia et écrire dans couchdb
2) proxy cross-domain (à cause des ports)
3) server web (inclut depuis PHP 5.4)

CouchApp
--------
Les points 2 et 3 de la section Remplacer tout le PHP peuvent se résoudre si on remplace le serveur web en PHP par CouchDB lui-même, s'évitant le problème de cross-domain.

Le scraper, quant à lui, ne sert qu'à ajouter des documents de tests à la DB. Ce n'est pas quelque chose qui doit rouler souvent. Autrement dit, cette dépendance à PHP n'est pas bien grave pour le moment.

CouchDB Admin party
-------------------
La config actuelle ne demande aucune autorisation (admin party). Il n'y a pas de login, tous ceux qui vont se connecter pourront créer des tables, effacer, etc. C'est actuellement caché par le proxy en PHP, mais bon. Il faudra tenir compte des utilisateurs et des droits d'accès et d'écriture bientôt.

Voir aussi <http://stackoverflow.com/questions/1923352/how-to-secure-couchdb> à propos des droits. Entre autre, _all_docs n'est accessibles qu'aux admins, selon ce document.

DRY [DONE]
----------
Beaucoup de code est dupliqué dans my.js. C'est maintenant arrangé :-)

Les requêtes CouchDB
--------------------
Le JavaScript actuel utilise uniquement la vue _all_docs et les arguments qui s'y rattachent. L'autre option offre plus de flexibilité, c'est à dire des vues que l'on conçoit en définissant des pairs de fonctions map() et reduce().

Pager / loading... [DONE]
-------------------------
Afficher un indice du progrès lors du chargement des prochains documents, quand à clique sur "next page". C'est maintenant fait :-)

Affichage par section
---------------------
Tous les documents ont un champ "section". Le frontpage affiche un item de chaque section. Chaque section a aussi sa page, qui affiche uniquement des documents de sa section.

La version actuelle affiche tous les documents dans le frontpage, sans indice sur la section. Les sections n'ont pas de pages.

AngularJS
---------
Le JavaScript actuel utilise du jQuery uniquement et offre une simple interface qui affiche 10 items par page et un bouton pour aller aux 10 suivants en effacant graduellement les items courants. Le bouton Back du fureteur ne permet pas actuellement de revenir aux 10 items précédents.

AngularJS va permettre de constuire quelque chose de plus solide. Occasion de tester le nouveau angular yeoman generator.

Twitter Bootstrap
-----------------
La version actuelle n'offre aucun layout, aucune classe sémantique et pire, hardcode les styles. Inclure Bootstrap pour faire une version responsive.

Caching proxy / load balancing
------------------------------
* Varnish...
* CouchDB replication

Performance measurements
------------------------
Un peu de phantomjs pourrait aller loin. voir le script loadspeed.js dans les examples de phantomjs. On peut aller plus loin cependant avec phantomas <https://github.com/macbre/phantomas>.

Avec 50 items, j'obtiens ces résultats. Le port 5984 c'est couchdb directement tandis que 8080 passe par un maigre proxy fait en php et c'est ce proxy qui parle à couchdb. Donc, pourquoi est-ce plus long directement, sur le port 5984, et plus rapide quand on passe par un intermédiaire? Peut-être justemment parce que la requete du html et du javascript prennent tous deux plus de temps par couchdb que par le serveur web sur le port 8080? Il faudra mesurer les temps avec wget pour comparer, c'est à dire le chargement du html uniquement.

Voici les temps pour une interprétation du javascript avec phantomjs. Dans tous les cas, la taille du document est de 3282822 octets.

	$ phantomjs loadspeed.js http://192.168.17.142:5984/journal_v2/_design/myapp/_rewrite/
	Loading time 15924 msec
	Loading time 12298 msec (2e test)

	$ phantomjs loadspeed.js http://192.168.17.142:8080/
	Loading time 11927 msec
	Loading time 11695 msec (2e test)

Quand on a le premier loading time, il faudrait faire un "next" pour timer aussi le paging.

Traitement input (rss, markup)
------------------------------
Tous les documents nous arrivent via Superfeedr. Voir le genre de normalisation qu'il fait aux données.

Traitement output (teaser, document complet)
--------------------------------------------
Hmm, est-ce que les fonctions "map" des views couchdb peuvent servir à trimmer (par exemple) le contenu entre la db et le client web?

Upgrade CouchDB sur Debian Squeeze
----------------------------------
* <http://www.freshblurbs.com/blog/2011/08/28/install-couchdb-debian-squeeze-source.html>
* <http://wiki.apache.org/couchdb/Installing_on_Debian>
* <http://wiki.apache.org/couchdb/PerDocumentAuthorization>

Partager ceci sur gitorious
---------------------------
S'ra pas long...

couchdb rewrite / vhosts
------------------------
Est-ce qu'on peut avoir quelque chose de plus court, plus beau que <http://127.0.0.1:5984/journal_v2/_design/myapp/_rewrite/> ?

erica / couchapp
----------------
Pour utiliser couchdb comme serveur web pour notre html et javascript côté client, il faut les outils erica et couchapp pour pousser nos fichiers dans couchdb. erica remplace couchapp mais n'a pas beaucoup de docs.
