<?php

class WikipediaFetcher implements JsonSerializable {
	const FAKE = true;
	private $page;
	private $language;
	private $url;
	private $html;
	private $edit;
	private $date;
	private $section;
	
	function __construct($page, $section, $language = 'fr') {
		$this->page = $page;
		$this->section = $section;
		$this->language = $language;
		$this->url = "http://{$this->language}.wikipedia.org/wiki/{$this->page}";
	}

	function jsonSerialize() {
		$obj = new stdclass;
		$obj->html = $this->html;
		$obj->edit = $this->edit;
		$obj->page = $this->page;
		$obj->language = $this->language;
		$obj->url = $this->url;
		$obj->date = $this->date;
		$obj->section = $this->section;
		$obj->license = 'http://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License';

// trying to cast instead of all the lines above, but...
//		$obj = (stdclass)$this;
		return $obj;
	}

	private function getXPath($url, $xpath) {
		$doc = new DOMDocument;
		$doc->preserveWhiteSpace = false;
		$doc->Load($url);
		$xpathobj = new DOMXPath($doc);
		return $doc->saveHTML($xpathobj->query($xpath)->item(0));
	}

	function getFake() {
		return $this->get(WikipediaFetcher::FAKE);
	}

	function get($fake = false) {
		$html_url = $fake ? 'html.html' : $this->url;
		$edit_url = $fake ? 'edit.html'
			: "http://{$this->language}.wikipedia.org/w/index.php?action=edit&title={$this->page}";

		$this->html = $this->getXPath($html_url, '//div[@id="mw-content-text"]');
//		$this->edit = $this->getXPath($edit_url, '//textarea[@id="wpTextbox1"]');
		$this->date = time();
		return array('html' => $this->html, 'edit' => $this->edit);	
	}
}
