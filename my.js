// when the html document is ready
$(function () {
    var tbl;
    tbl = 'journal_v2';

	$('<div/>', {id: 'holdon', html: 'hold on, loading page...'}).appendTo('body');;
	$('<div/>', {id: 'stuff'}).appendTo('body');

    function doit(tbl, startkey_docid) {
        var sdi;

        sdi = startkey_docid ? ('&startkey_docid=' + startkey_docid) : '';

        // see previous getJSON() call to explain proxy.php
        // _all_docs is a magic string, returning all documents in the table
        // see http://wiki.apache.org/couchdb/HTTP_view_API
        // by default, it only returns ids and revisions
        // so each rows item contains a key and rev.
        // Here, we want everything in the document, not just its id,
        // so we use the "include_docs=true" query string.
        // Now the rows items will also contain a "doc" key, holding
        // all the doc fields.
        $.getJSON('/proxy.php/5984/' + tbl + '/_all_docs?include_docs=true&limit=11' + sdi, function (data) {
            var it, r;

			$('#holdon').fadeOut();
            for (r = 0; r < data.rows.length - 1; ++r) {
                if (data.rows[r].doc && data.rows[r].doc.html) {
//                    it = '<div style="border: thin dotted green; font-size: 70%; width: 12em; height: 17em; float: left; padding: 1em; margin: 1em; overflow: hidden"><h3>' + data.rows[r].key + ' (' + data.rows[r].value.rev + ')</h3>';
                    it = '<div style="border: thin dotted green; font-size: 70%; width: 12em; height: 17em; float: left; padding: 1em; margin: 1em; overflow: hidden"><h3>' + data.rows[r].key + '</h3>';
                    if (data.rows[r].doc.section) {
						it += '<h4>' + data.rows[r].doc.section + '</h4>';
					}
                    it += data.rows[r].doc.html;
                    it += '</div>';

                    $('<div/>', {
                        html: it
                    }).appendTo('#stuff').addClass('item');
                }
            }
            r = data.rows.length - 1;
            if (data.rows[r].key && data.rows[r].doc && data.rows[r].doc.html) {
                it = '<a id="nextpage" href="#next-' + data.rows[r].key + '" data-next=' + data.rows[r].key + '>next</a>';
                $('<div/>', {
                    html: it
                }).appendTo('#stuff').addClass('item');
            }
        });
    }

    $('body').on('click', 'a#nextpage', function (e) {
        $('#stuff').fadeOut('fast', function() {
			var x;
			$('#holdon').fadeIn('fast');
			$('#stuff').html('').fadeIn('fast');
			startkey_docid = $(e.target).data('next');
			doit(tbl, startkey_docid);
		});
    });
    doit(tbl, 0)
});
