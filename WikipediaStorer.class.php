<?php

class WikipediaStorer {
	private $db;

	function __construct($db = 'http://localhost:5984/journal_v2') {
		$this->db = $db;
	}
	
	function store(JsonSerializable $wpp) {
		$params = array('http' => array(
			'method' => 'POST',
			'header'=>"Content-Type: application/json\r\n",
			'content' => json_encode($wpp)
		));

		$ctx = stream_context_create($params);
		return json_decode(file_get_contents($this->db, 0, $ctx));
	}
}
