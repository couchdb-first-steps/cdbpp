<?php

//print_r($_SERVER);
//die();

// $_SERVER['PATH_INFO'] is everything in
// the URL after "http://localhost:8080/proxy.php"
$p = explode('/', ltrim($_SERVER['PATH_INFO'], '/'));

// take '/proxy.php/5984/journal_v1' for example
// the first argument (between "/") must be numerical
// representing an http port
// fail otherwise
if (!is_numeric($p[0])) {
  header('x-ouch', true, 503);
  die();
}

$port = $p[0];
unset($p[0]);

// reformat the URL
$url = "http://localhost:$port/" . join('/', $p);

switch($_SERVER['REQUEST_METHOD']) {
	case 'GET':
		// add query string if necessary
		if (!empty($_SERVER['QUERY_STRING'])) $url .= '?' . $_SERVER['QUERY_STRING'];

		// voilà, return the content
		echo file_get_contents($url);
		break;

	case 'PUT':
		// we want to http PUT
		$http = stream_context_create(array('http'=>array('method'=>'PUT')));		
		$resp = file_get_contents($url, false, $http);
		// $http_response_header is magic
		// see http://ca.php.net/manual/en/reserved.variables.httpresponseheader.php
		$code = $http_response_header[0];
		
		// let's handle the simple expected case
		// where HTTP begings the first header (response code)
		if ('HTTP' !== substr($code, 0, 4)) {
		  header('x-ouch:nothttp', true, 503);
		  die();
		}
		// 201 means we create whatever
		if (strpos($code, '201')) {
		  header('Created', 201);
		  echo $resp;
		} else {
		  // otherwise, we say we failed
		  // maybe the whatever already exists?
		  header('x-ouch:Precondition Failed', 412);
		  die();
		}
		break;
}
